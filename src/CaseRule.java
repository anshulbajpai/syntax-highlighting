public class CaseRule extends Rule {
    private final Case aCase;

    private CaseRule(Case aCase, Rule rule) {
        super(rule);
        this.aCase = aCase;
    }

    private CaseRule(Case aCase) {
        this(aCase,NULL);
    }

    public static CaseRule with(Case aCase) {
        return new CaseRule(aCase);
    }

    @Override
    public String applyOn(String word) {
        return aCase.convert(word);
    }
}
