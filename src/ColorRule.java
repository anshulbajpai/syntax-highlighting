public class ColorRule extends Rule {
    private final String color;

    private ColorRule(String color, Rule rule) {
        super(rule);
        this.color = color;
    }

    private ColorRule(String color) {
        this(color,NULL);
    }

    public static ColorRule with(String color, Rule rule) {
        return new ColorRule(color, rule);
    }

    public static ColorRule with(String color) {
        return new ColorRule(color);
    }


    private String getColorHighlighter() {
        return String.format("[%s]", color);
    }

    @Override
    public String applyOn(String word) {
        return getColorHighlighter()+ apply(word) + getColorHighlighter();
    }

}
