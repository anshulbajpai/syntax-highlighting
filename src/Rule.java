public abstract class Rule {
    public static final Rule NULL = new NullRule();
    protected final Rule rule;

    public Rule(Rule rule) {
        this.rule = rule;
    }

    public abstract String applyOn(String word);

    protected String apply(String word) {
        return rule.applyOn(word);
    }
}
