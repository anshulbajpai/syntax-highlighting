public enum Case {
    LOWER {
        @Override
        String convert(String word) {
           return word.toLowerCase();
        }
    }, NORMAL {
        @Override
        String convert(String word) {
            return word;
        }
    }, CAPITAL {
        @Override
        String convert(String word) {
            return word.toUpperCase();
        }
    };

    abstract String convert(String word);
}
