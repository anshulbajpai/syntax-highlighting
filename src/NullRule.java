public class NullRule extends Rule {
    public NullRule() {
        super(NULL);
    }

    @Override
    public String applyOn(String word) {
        return word;
    }
}
