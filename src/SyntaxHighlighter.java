import java.util.List;

public class SyntaxHighlighter {
    private static final String SPACE = " ";
    private final List<KeyWord> keyWords;

    public SyntaxHighlighter(List<KeyWord> keyWords) {
        this.keyWords = keyWords;
    }

    public static SyntaxHighlighter withRules(List<KeyWord> keywords) {
        return new SyntaxHighlighter(keywords);
    }

    public String highlight(String input) {
        String result =  applyOn(input.split(SPACE));
        return result.substring(0,result.lastIndexOf(SPACE));
    }

    private String applyOn(String[] words) {
        StringBuffer buffer = new StringBuffer();
        for (String word : words) {
            buffer.append(applyOn(word)).append(SPACE);
        }
        return buffer.toString();
    }

    private String applyOn(String word) {
        for (KeyWord keyWord : keyWords) {
            if(keyWord.matches(word)){
                return keyWord.applyRuleOn(word);
            }
        }
        return word;
    }


}