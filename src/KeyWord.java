public class KeyWord{

    private final String word;
    private final Rule rule;

    private KeyWord(String word, Rule rule) {
        this.word = word;
        this.rule = rule;
    }

    public static KeyWord _for(String word, Rule rule) {
        return new KeyWord(word, rule);
    }

    public boolean matches(String word){
        return word.toLowerCase().equals(this.word);
    }

    public String applyRuleOn(String word) {
        return rule.applyOn(word);
    }
}
