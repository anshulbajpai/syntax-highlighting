public class StyleRule extends Rule{

    private final Style style;

    public StyleRule(Style style, Rule rule) {
        super(rule);
        this.style = style;
    }

    @Override
    public String applyOn(String word) {
        return style.apply(apply(word));
    }

    public static Rule with(Style style, Rule rule) {
        return new StyleRule(style, rule);
    }
}
