public enum Style {
    BOLD {
        @Override
        public String apply(String word) {
            return String.format("[bold]%s[bold]",word);
        }
    }, NORMAL {
        @Override
        public String apply(String word) {
            return word;
        }
    };


    public abstract String apply(String word);
}
