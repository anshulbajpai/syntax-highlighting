import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class RuleSpecs {

    @Test
    public void itAppliesRule() {
        assertThat(ColorRule.with("blue", CaseRule.with(Case.LOWER)).applyOn("Anshul")).isEqualTo("[blue]anshul[blue]");
    }
}
