import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class SyntaxHighlighterSpecs  {

    @Test
    public void itAddSameColorToAllKeywords() {

        List<KeyWord> colors = new ArrayList<KeyWord>();
        colors.add(KeyWord._for("as", ColorRule.with("blue")));
        colors.add(KeyWord._for("if", ColorRule.with("blue")));
        colors.add(KeyWord._for("and", ColorRule.with("blue")));
        colors.add(KeyWord._for("then", ColorRule.with("blue")));
        colors.add(KeyWord._for("when", ColorRule.with("blue")));
        String output = SyntaxHighlighter.withRules(colors).highlight("If we write a program and compile it, then we can run the program to get output");
        assertThat(output).isEqualTo("[blue]If[blue] we write a program [blue]and[blue] compile it, [blue]then[blue] we can run the program to get output");
    }

    @Test
    public void itHighlightsKeywordsWithRespectiveColors() {
        List<KeyWord> colors = new ArrayList<KeyWord>();
        colors.add(KeyWord._for("as", ColorRule.with("blue")));
        colors.add(KeyWord._for("if", ColorRule.with("red")));
        colors.add(KeyWord._for("and", ColorRule.with("red")));
        colors.add(KeyWord._for("then", ColorRule.with("green")));
        colors.add(KeyWord._for("when", ColorRule.with("blue")));

        String output = SyntaxHighlighter.withRules(colors).highlight("If we write a program and compile it, then as we run the program, we will get output");
        assertThat(output).isEqualTo("[red]If[red] we write a program [red]and[red] compile it, [green]then[green] [blue]as[blue] we run the program, we will get output");
    }

    @Test
    public void itHighlightKeywordsWithCasing() {

        List<KeyWord> colors = new ArrayList<KeyWord>();
        colors.add(KeyWord._for("as", ColorRule.with("blue", CaseRule.with(Case.CAPITAL))));
        colors.add(KeyWord._for("if", ColorRule.with("red", CaseRule.with(Case.LOWER))));
        colors.add(KeyWord._for("and", ColorRule.with("red", CaseRule.with(Case.CAPITAL))));
        colors.add(KeyWord._for("then", ColorRule.with("green", CaseRule.with(Case.LOWER))));
        colors.add(KeyWord._for("when", ColorRule.with("blue", CaseRule.with(Case.LOWER))));

        String output = SyntaxHighlighter.withRules(colors).highlight("If we write a program and compile it, then as we run the program, we will get output");
        assertThat(output).isEqualTo("[red]if[red] we write a program [red]AND[red] compile it, [green]then[green] [blue]AS[blue] we run the program, we will get output");
    }

    @Test
    public void itHighlightsStyle() {

        List<KeyWord> colors = new ArrayList<KeyWord>();
        colors.add(KeyWord._for("as", ColorRule.with("blue", StyleRule.with(Style.NORMAL, CaseRule.with(Case.CAPITAL)))));
        colors.add(KeyWord._for("if", ColorRule.with("red", StyleRule.with(Style.BOLD, CaseRule.with(Case.LOWER)))));
        colors.add(KeyWord._for("and", ColorRule.with("red", StyleRule.with(Style.BOLD, CaseRule.with(Case.CAPITAL)))));
        colors.add(KeyWord._for("then", ColorRule.with("green", StyleRule.with(Style.NORMAL, CaseRule.with(Case.LOWER)))));
        colors.add(KeyWord._for("when", ColorRule.with("blue", StyleRule.with(Style.NORMAL, CaseRule.with(Case.LOWER)))));

        String output = SyntaxHighlighter.withRules(colors).highlight("If we write a program and compile it, then as we run the program, we will get output");
        assertThat(output).isEqualTo("[red][bold]if[bold][red] we write a program [red][bold]AND[bold][red] compile it, [green]then[green] [blue]AS[blue] we run the program, we will get output");
    }

}
